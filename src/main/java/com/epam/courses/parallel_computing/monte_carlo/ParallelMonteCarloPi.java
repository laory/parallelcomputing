package com.epam.courses.parallel_computing.monte_carlo;

import java.text.DecimalFormat;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/18/2014
 * Time: 11:07 AM
 */
public class ParallelMonteCarloPi {

    private static final int ITERATIONS = 1_000_000_000;
    private final int threads;

    public ParallelMonteCarloPi(int threads) {
        this.threads = threads;
    }

    public double calculatePi() {
        int globalCounter = 0;
        HitPointsCalculator[] calculationThreads = prepareThreads();

        for (HitPointsCalculator calculationThread : calculationThreads) {
            calculationThread.start();
        }

        for (HitPointsCalculator calculationThread : calculationThreads) {
            try {
                calculationThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (HitPointsCalculator calculationThread : calculationThreads) {
            globalCounter += calculationThread.counter;
        }

        return 4.0 * globalCounter / ITERATIONS;
    }

    private HitPointsCalculator[] prepareThreads() {
        HitPointsCalculator[] calculators = new HitPointsCalculator[threads];
        int threadIterations = ITERATIONS / threads;

        for (int i = 0; i < threads - 1; i++) {
            calculators[i] = new HitPointsCalculator(threadIterations);
        }
        calculators[threads - 1] = new HitPointsCalculator(ITERATIONS - threadIterations * (threads - 1));

        return calculators;
    }

    private class HitPointsCalculator extends Thread {

        private final int threadIterations;
        private       int counter;

        public HitPointsCalculator(int threadIterations) {
            this.threadIterations = threadIterations;
            this.counter = 0;
        }

        @Override
        public void run() {
            for (int i = 0; i < threadIterations; i++) {
                double xPoint = ThreadLocalRandom.current().nextDouble();
                double yPoint = ThreadLocalRandom.current().nextDouble();
                double squareDistance = Math.pow(xPoint, 2) + Math.pow(yPoint, 2);
                if (Double.compare(squareDistance, 1.0) <= 0) {
                    counter++;
                }
            }
        }
    }

    public static void main(String[] args) {
        int threads = Integer.valueOf(args[0]);

        ParallelMonteCarloPi parallelMonteCarloPi = new ParallelMonteCarloPi(threads);
        long start = System.currentTimeMillis();
        double pi = parallelMonteCarloPi.calculatePi();
        long executionTime = System.currentTimeMillis() - start;

        System.out.println("PI is " + new DecimalFormat("#.00000").format(pi));
        System.out.println("THREADS " + args[0]);
        System.out.println("ITERATIONS " + new DecimalFormat("#,###").format(ParallelMonteCarloPi.ITERATIONS));
        System.out.println("TIME " + new DecimalFormat("#.00").format(executionTime / 1000.0) + "s");
    }
}