package com.epam.courses.parallel_computing.parallel_merge_sort;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/29/2014
 * Time: 11:16 AM
 */
public class ParallelMergeSorter {

    /**
     * Sorts supplied array using parallel merge sort algorithm.
     * Parallel merge sort algorithm implemented as is (didn't want to fuck up indices, so use redundant arrays:
     * [0, i0, i1, ..., in-1], it allows to access array cells by indices from 1 to n).
     *
     * @see <a href="http://10.6.132.148/c4x/EPAM/PC101/asset/9780262033848_sch_0001.pdf">http://10.6.132.148/c4x/EPAM/PC101/asset/9780262033848_sch_0001.pdf</a>
     */
    public static int[] sort(int[] arrayToSort) {

        int[] inputArray = new int[arrayToSort.length + 1];
        int[] sortedArray = new int[inputArray.length];
        int[] result = new int[arrayToSort.length];

        System.arraycopy(arrayToSort, 0, inputArray, 1, arrayToSort.length);
        parallelMergeSort(inputArray, 1, arrayToSort.length, sortedArray, 1);
        System.arraycopy(sortedArray, 1, result, 0, arrayToSort.length);

        return result;
    }

    private static void parallelMergeSort(final int[] A, final int p, int r, int[] B, int s) {
        int n = r - p + 1;
        if (n == 1) {
            B[s] = A[p];
        } else {
            final int[] T = new int[n + 2];
            final int q = (p + r) / 2;
            int qQuote = q - p + 1;

            Thread newThread = new Thread() {
                @Override
                public void run() {
                    parallelMergeSort(A, p, q, T, 1);
                }
            };
            newThread.start();
            parallelMergeSort(A, q + 1, r, T, qQuote + 1);

            try {
                newThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            parallelMerge(T, 1, qQuote, qQuote + 1, n, B, s);
        }
    }

    private static void parallelMerge(final int[] T, int p1, int r1, int p2, int r2, final int[] A, final int p3) {
        int n1 = r1 - p1 + 1;
        int n2 = r2 - p2 + 1;
        if (n1 < n2) {
            int temp;
            temp = p1;
            p1 = p2;
            p2 = temp;

            temp = r1;
            r1 = r2;
            r2 = temp;

            n1 = n2;
        }
        if (n1 != 0) {
            final int q1 = (p1 + r1) / 2;
            final int q2 = binarySearch(T[q1], T, p2, r2);
            int q3 = p3 + (q1 - p1) + (q2 - p2);
            A[q3] = T[q1];

            final int finalP1 = p1;
            final int finalP2 = p2;
            Thread newThread = new Thread() {
                @Override
                public void run() {
                    parallelMerge(T, finalP1, q1 - 1, finalP2, q2 - 1, A, p3);
                }
            };
            newThread.start();

            parallelMerge(T, q1 + 1, r1, q2, r2, A, q3 + 1);

            try {
                newThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static int binarySearch(int x, int[] T, int p, int r) {
        int low = p;
        int high = Math.max(p, r + 1);
        while (low < high) {
            int mid = (low + high) / 2;
            if (x <= T[mid]) {
                high = mid;
            } else {
                low = mid + 1;
            }
        }
        return high;
    }

    public static void main(String[] args) {
        int[] testArray = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -6, -7, -8, -9};
        System.out.println(Arrays.toString(sort(testArray)));
    }
}
