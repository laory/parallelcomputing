package com.epam.courses.parallel_computing.producer_consumer.producers;

import com.epam.courses.parallel_computing.bank.Bank;
import com.epam.courses.parallel_computing.producer_consumer.tasks.MoneyTransfer;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.concurrent.ExecutorService;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/3/2014
 * Time: 1:05 PM
 */
public class TransferProducer implements EntityProducer<MoneyTransfer> {

    private final Bank            bank;
    private final ExecutorService executor;

    public TransferProducer(Bank bank, ExecutorService executor) {
        this.bank = bank;
        this.executor = executor;
    }

    @Override
    public MoneyTransfer[] produceEntities(DataInputStream dataInputStream, int limit, int offset) throws IOException {
        MoneyTransfer[] moneyTransfers = new MoneyTransfer[limit];
        int skippedBytes = dataInputStream.skipBytes(offset);
        if (skippedBytes != offset) {
            throw new IOException("Unable to skip " + offset + " bytes in data stream");
        }
        try {
            for (int transfersCounter = 0; transfersCounter < limit; transfersCounter++) {
                int fromAccountSn = dataInputStream.readInt();
                int toAccountSn = dataInputStream.readInt();
                int amount = dataInputStream.readInt();
                MoneyTransfer moneyTransfer = new MoneyTransfer(bank, fromAccountSn, toAccountSn, amount);
                executor.submit(moneyTransfer);
                moneyTransfers[transfersCounter] = moneyTransfer;
            }
        } catch (EOFException e) {
            throw new IllegalStateException("Corrupted input data");
        }
        return moneyTransfers;
    }
}
