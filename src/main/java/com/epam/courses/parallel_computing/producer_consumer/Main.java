package com.epam.courses.parallel_computing.producer_consumer;

import com.epam.courses.parallel_computing.bank.Account;
import com.epam.courses.parallel_computing.bank.Bank;
import com.epam.courses.parallel_computing.producer_consumer.producers.AccountsProducer;
import com.epam.courses.parallel_computing.producer_consumer.producers.TransferProducer;
import com.epam.courses.parallel_computing.producer_consumer.tasks.MoneyTransfer;

import java.io.*;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/6/2014
 * Time: 11:31 AM
 */
public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("com.epam.courses.parallel_computing.twitter.Main thread id: " + Thread.currentThread().getId());
        ExecutorService executor = new ThreadPoolExecutor(15, // core pool size
                                                          30, // max pool size
                                                          100L, TimeUnit.MILLISECONDS, // keep alive
                                                          new LinkedBlockingQueue<Runnable>(100_000),
                                                          // queue
                                                          new ThreadPoolExecutor.CallerRunsPolicy()); // handler

        AccountsProducer accountProducer = new AccountsProducer();
        try {
            FileInputStream fileInputStream = new FileInputStream(
                    "C:\\Users\\Dmytro_Babichev\\IdeaProjects\\ParallelComputing\\src\\main\\resources\\input");
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);

            int accountsNumber = dataInputStream.readInt();
            Account[] accounts = accountProducer.produceEntities(dataInputStream, accountsNumber, 0);
            dataInputStream.close();
            System.out.println("All accounts are ready...");
            System.out.println("Accounts number: " + accountsNumber);
            //            System.out.println(Arrays.toString(accounts));

            Bank bank = new Bank(accounts);
            TransferProducer transferProducer = new TransferProducer(bank, executor);

            long start = System.currentTimeMillis();
            fileInputStream = new FileInputStream(
                    "C:\\Users\\Dmytro_Babichev\\IdeaProjects\\ParallelComputing\\src\\main\\resources\\input");
            dataInputStream = new DataInputStream(fileInputStream);
            dataInputStream.skipBytes((accountsNumber + 1) * 4);
            int transfersNumber = dataInputStream.readInt();
            MoneyTransfer[] moneyTransfers = transferProducer.produceEntities(dataInputStream, transfersNumber, 0);
            dataInputStream.close();
            System.out.println("All money transfers are ready...");
            System.out.println("Transfers number: " + transfersNumber);
            //            System.out.println(Arrays.toString(moneyTransfers));

            executor.shutdown(); // Disable new tasks from being submitted
            try {
                // Wait a while for existing tasks to terminate
                if (!executor.awaitTermination(60 * 2, TimeUnit.SECONDS)) {
                    executor.shutdownNow(); // Cancel currently executing tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!executor.awaitTermination(60 * 2, TimeUnit.SECONDS)) {
                        System.err.println("Pool did not terminate");
                    }
                }
            } catch (InterruptedException ie) {
                // (Re-)Cancel if current thread also interrupted
                executor.shutdownNow();
                // Preserve interrupt status
                Thread.currentThread().interrupt();
            }
            System.out.println(System.currentTimeMillis() - start);

            try {
                FileReader taskReader = new FileReader(
                        "C:\\Users\\Dmytro_Babichev\\IdeaProjects\\ParallelComputing\\src\\main\\resources\\output");
                BufferedReader bufferedReader = new BufferedReader(taskReader);

                String result = bufferedReader.readLine();
                System.out.println(
                        result.equals(Arrays.toString(accounts)) ? "The results are correct" : "Wrong results");
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (EOFException e) {
            throw new IllegalStateException("Corrupted input data");
        }
    }
}
