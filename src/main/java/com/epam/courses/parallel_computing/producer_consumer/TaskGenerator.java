package com.epam.courses.parallel_computing.producer_consumer;

import com.epam.courses.parallel_computing.bank.Account;

import java.io.*;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/7/2014
 * Time: 10:33 AM
 */
public class TaskGenerator {

    public static void generateTaskAndWriteToFile(String path) {
        try {
            int accountsNumber = 100_000;
            int transfersNumber = 100_000;
            Account[] accounts = new Account[accountsNumber];

            FileOutputStream fileOutputStream = new FileOutputStream(path);
            DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream);

            dataOutputStream.writeInt(accountsNumber);
            for (int i = 0; i < accountsNumber; i++) {
                int money = ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE);
                accounts[i] = new Account(i, money);
                dataOutputStream.writeInt(money);
                dataOutputStream.flush();
            }
            dataOutputStream.writeInt(transfersNumber);

            for (int i = 0; i < transfersNumber; i++) {
                int from = ThreadLocalRandom.current().nextInt(accountsNumber);
                int to = ThreadLocalRandom.current().nextInt(accountsNumber);
                if (to == from) {
                    i--;
                    continue;
                }
                int amount = ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE);
                accounts[from].withdraw(amount);
                accounts[to].deposit(amount);
                dataOutputStream.writeInt(from);
                dataOutputStream.writeInt(to);
                dataOutputStream.writeInt(amount);
                dataOutputStream.flush();
            }
            dataOutputStream.close();

            int lastBackSlash = path.lastIndexOf("\\") + 1;
            FileWriter fileWriter = new FileWriter(path.substring(0, lastBackSlash) + "output");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(Arrays.toString(accounts));
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        generateTaskAndWriteToFile(
                "C:\\Users\\Dmytro_Babichev\\IdeaProjects\\ParallelComputing\\src\\main\\resources\\input");
    }
}
