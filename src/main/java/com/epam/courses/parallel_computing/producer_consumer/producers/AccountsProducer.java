package com.epam.courses.parallel_computing.producer_consumer.producers;

import com.epam.courses.parallel_computing.bank.Account;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/3/2014
 * Time: 1:00 PM
 */
public class AccountsProducer implements EntityProducer<Account> {

    @Override
    public Account[] produceEntities(DataInputStream dataInputStream, int limit, int offset) throws IOException {
        Account[] accounts = new Account[limit];
        int skippedBytes = dataInputStream.skipBytes(offset);
        if (skippedBytes != offset) {
            throw new IOException("Unable to skip " + offset + " bytes in data stream");
        }
        try {
            for (int accountsCounter = 0; accountsCounter < limit; accountsCounter++) {
                accounts[accountsCounter] = new Account(accountsCounter, dataInputStream.readInt());
            }
        } catch (EOFException e) {
            throw new IllegalStateException("Corrupted input data");
        }
        return accounts;
    }
}
