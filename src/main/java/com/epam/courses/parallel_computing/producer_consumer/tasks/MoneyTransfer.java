package com.epam.courses.parallel_computing.producer_consumer.tasks;

import com.epam.courses.parallel_computing.bank.Account;
import com.epam.courses.parallel_computing.bank.Bank;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/3/2014
 * Time: 11:50 AM
 */
public class MoneyTransfer implements Runnable {

    private final Bank    bank;
    private final Account fromAccount;
    private final Account toAccount;

    public final  int     fromAccountSn;
    public final  int     toAccountSn;
    public final  int     amount;

    public MoneyTransfer(Bank bank, int fromAccount, int toAccount, int amount) {
        this.bank = bank;
        this.fromAccount = bank.getAccount(fromAccount);
        this.toAccount = bank.getAccount(toAccount);

        this.amount = amount;
        this.fromAccountSn = fromAccount;
        this.toAccountSn = toAccount;
    }

    @Override
    public void run() {
        try {
            bank.transfer(fromAccount, toAccount, amount);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "MoneyTransfer{" +
               "fromAccountSn=" + fromAccountSn +
               ", toAccountSn=" + toAccountSn +
               ", amount=" + amount +
               '}';
    }
}
