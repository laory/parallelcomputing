package com.epam.courses.parallel_computing.producer_consumer.producers;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/3/2014
 * Time: 12:55 PM
 */
public interface EntityProducer<T> {

    public T[] produceEntities(DataInputStream dataInputStream, int limit, int offset) throws IOException;
}
