package com.epam.courses.parallel_computing.interview;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Task 1.
 *
 * Created by Hedgehog on 23/6/15.
 */
public class Account {

    public Account(int money) {}

    public void withdraw(int amount) {
        throw new UnsupportedOperationException();
    }

    public void deposit(int amount) {
        throw new UnsupportedOperationException();
    }

    public int balance() {
        throw new UnsupportedOperationException();
    }
}

class AccountSolution1 {

    private volatile int money;

    public AccountSolution1(int money) {
        this.money = money;
    }

    public synchronized void withdraw(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Wrong number to withdraw. Amount: " + amount);
        }
        if (money < amount) {
            throw new IllegalArgumentException("Not enough money to perform withdraw. Amount: " + amount);
        }
        money -= amount;
    }

    public synchronized void deposit(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Wrong number to deposit. Amount: " + amount);
        }
        money += amount;
    }

    public int balance() {
        return money;
    }
}

class AccountSolution2 {

    private final AtomicInteger money;

    public AccountSolution2(int money) {
        this.money = new AtomicInteger(money);
    }

    public void withdraw(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Wrong number to withdraw. Amount: " + amount);
        }
        if (money.get() < amount) {
            throw new IllegalArgumentException("Not enough money to perform withdraw. Amount: " + amount);
        }
        money.addAndGet(-amount);
    }

    public void deposit(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Wrong number to deposit. Amount: " + amount);
        }
        money.addAndGet(amount);
    }

    public int balance() {
        return money.get();
    }
}
