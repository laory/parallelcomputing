package com.epam.courses.parallel_computing.interview;

/**
 * Created by Hedgehog on 24/6/15.
 */
public final class DCL {

    private static DCL instance = null;

    public static DCL getInstance() {
        throw new UnsupportedOperationException();
    }
}

class DCLSolution1 {

    private DCLSolution1() {}

    private static volatile DCLSolution1 instance = null;

    public static DCLSolution1 getInstance() {
        if (instance == null) {
            synchronized (DCLSolution1.class) {
                if (instance == null)
                    instance = new DCLSolution1();
            }
        }
        return instance;
    }
}

class Singleton {

    private static class Instance {
        static final Singleton value = new Singleton();
    }

    public static Singleton getInstance() {
        return Instance.value;
    }
}