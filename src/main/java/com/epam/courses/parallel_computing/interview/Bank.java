package com.epam.courses.parallel_computing.interview;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;

/**
 * Created by Hedgehog on 24/6/15.
 */
public class Bank {

    public void transfer(Account from, Account to, int amount) throws Exception {
        throw new UnsupportedOperationException();
    }

    public static class Account {

        private int money;

        public Account(int money) {
            this.money = money;
        }

        private void withdraw(int amount) {
            money -= amount;
        }

        private void deposit(int amount) {
            money += amount;
        }

        public int balance() {
            return money;
        }
    }
}

class BankSolution1 {

    private static final int RETRIES = 5;

    public void transfer1(Account from, Account to, int amount) throws IllegalMonitorStateException,
                                                                       InterruptedException {
        boolean finished = false;
        int tries = RETRIES;
        while (!finished && tries-- > 0) {
            if (from.lock.tryLock() || from.lock.tryLock(50, TimeUnit.MILLISECONDS)) {
                try {
                    if (to.lock.tryLock() || to.lock.tryLock(50, TimeUnit.MILLISECONDS)) {
                        try {
                            from.withdraw(amount);
                            to.deposit(amount);
                            finished = true;
                            sleep(100); // do work
                        } finally {
                            to.lock.unlock();
                        }
                    }
                } finally {
                    from.lock.unlock();
                }
            }
        }
        if (!finished && tries == -1) {
            throw new IllegalMonitorStateException();
        }
    }

    public static class Account {

        public final ReentrantLock lock = new ReentrantLock();
        private volatile int money;

        public Account(int money) {
            this.money = money;
        }

        private void withdraw(int amount) {
            money -= amount;
        }

        private void deposit(int amount) {
            money += amount;
        }

        public int balance() {
            return money;
        }
    }
}

class BankSolution2 {

    public void transfer2(Account from, Account to, int amount) throws InterruptedException {
        Account firstLock;
        Account secondLock;
        if (from.sn <= to.sn) {
            firstLock = from;
            secondLock = to;
        } else {
            firstLock = to;
            secondLock = from;
        }
        synchronized(firstLock) {
            synchronized(secondLock) {
                from.withdraw(amount);
                to.deposit(amount);
                sleep(100); // do work
            }
        }
    }

    public static class Account {

        public final     int sn;
        private volatile int money;

        public Account(int sn, int money) {
            this.sn = sn;
            this.money = money;
        }

        private void withdraw(int amount) {
            if (amount <= 0) {
                throw new IllegalArgumentException("Wrong number to withdraw. Amount: " + amount);
            }
            money -= amount;
        }

        private void deposit(int amount) {
            if (amount <= 0) {
                throw new IllegalArgumentException("Wrong number to deposit. Amount: " + amount);
            }
            money += amount;
        }

        public int balance() {
            return money;
        }
    }

}
