package com.epam.courses.parallel_computing.stm;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/21/2014
 * Time: 10:34 AM
 */
public interface CircularBuffer<E> {
    public E push(E item); // why the fuck this returns value and what this value is????
    public E pop();
}
