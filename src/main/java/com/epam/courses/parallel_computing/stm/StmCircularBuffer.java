package com.epam.courses.parallel_computing.stm;

import org.multiverse.api.functions.Function;
import org.multiverse.api.references.TxnInteger;
import org.multiverse.api.references.TxnRef;

import java.util.Arrays;
import java.util.concurrent.Callable;

import static org.multiverse.api.StmUtils.*;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/21/2014
 * Time: 10:39 AM
 */
public class StmCircularBuffer<T> implements CircularBuffer<T> {

    private final TxnRef<T[]> buffer;

    private final TxnInteger size;
    private final TxnInteger first;
    private final TxnInteger last;

    @SuppressWarnings("unchecked")
    public StmCircularBuffer(int size) {
        buffer = newTxnRef((T[]) (new Object[size + 1])); // one cell is redundant
        this.size = newTxnInteger(size);
        this.first = newTxnInteger(0);
        this.last = newTxnInteger(0);
    }

    public boolean isEmpty() {
        return atomic(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return last.get() == first.get();
            }
        });
    }

    public boolean isFull() {
        return atomic(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return increasedIndex(last) == first.get();
            }
        });
    }

    private int increasedIndex(final TxnInteger index) {
        return atomic(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return (index.get() + 1) % (size.get() + 1);
            }
        });
    }

    @Override
    public T push(final T item) {
        return atomic(new Callable<T>() {
            @Override
            public T call() throws Exception {
                T previousValue = buffer.get()[last.get()];
                buffer.atomicAlterAndGet(new Function<T[]>() {
                    @Override
                    public T[] call(T[] value) {
                        value[last.get()] = item;
                        value[increasedIndex(last)] = null;
                        return value;
                    }
                });
                if (isFull()) {
                    first.set(increasedIndex(first));
                }
                last.set(increasedIndex(last));
                return previousValue; // not sure if this was required
            }
        });
    }

    @Override
    public T pop() {
        return atomic(new Callable<T>() {
            @Override
            public T call() throws Exception {
                if (isEmpty()) {
                    return null;
                }
                T result = buffer.get()[first.get()];
                buffer.getAndAlter(new Function<T[]>() {
                    @Override
                    public T[] call(T[] value) {
                        value[first.get()] = null;
                        return value;
                    }
                });
                first.set(increasedIndex(first));
                return result;
            }
        });
    }

    @SuppressWarnings("unchecked")
    private T[] toArray(Class<Double[]> claz) {
        T[] buf = buffer.atomicGet();
        return (T[]) Arrays.copyOf(buf, buf.length, claz);
    }

    public static void main(String[] args) throws InterruptedException {
        final int size = 10_000_000;
        final int threadNumber = 100;
        final int pushOperationsCount = size / threadNumber;
        final StmCircularBuffer<Double> circularBuffer = new StmCircularBuffer<>(size);
        Thread[] threads = new Thread[threadNumber];
        for (int i = 0; i < threadNumber; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (double i = 0; i < pushOperationsCount + 100; i++) {
                        circularBuffer.push(i);
                    }
                }
            });
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
        int nullsCount = 0;
        for (Double num : circularBuffer.toArray(Double[].class)) {
            if (num == null) {
                nullsCount++;
            }
        }
        System.out.println("Push operation is " +
                           ((nullsCount == 1 && !circularBuffer.isEmpty() && circularBuffer.isFull()) ? "thread safe"
                                                                                                      : "not thread safe"));
        threads = new Thread[threadNumber];
        for (int i = 0; i < threadNumber; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (double i = 0; i < pushOperationsCount + 10; i++) {
                        circularBuffer.pop();
                    }
                }
            });
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
        nullsCount = 0;
        Double[] buffer = circularBuffer.toArray(Double[].class);
        for (Double num : buffer) {
            if (num == null) {
                nullsCount++;
            }
        }
        System.out.println("Pop operation is " +
                           ((nullsCount == buffer.length && circularBuffer.isEmpty() &&
                             !circularBuffer.isFull()) ? "thread safe" : "not thread safe"));
        int smallSize = 5;
        StmCircularBuffer<Double> smallCircularBuffer = new StmCircularBuffer<>(smallSize);
        System.out.println();
        System.out.println(Arrays.toString(smallCircularBuffer.buffer.atomicGet()));
        for (double i = 0; i < smallSize; i++) {
            smallCircularBuffer.push(i);
            System.out.println("Push: " + i);
        }
        System.out.println(Arrays.toString(smallCircularBuffer.buffer.atomicGet()));
        System.out.println();
        for (int i = 0; i < smallSize; i++) {
            System.out.println("Pop: " + smallCircularBuffer.pop());
        }
        System.out.println(Arrays.toString(smallCircularBuffer.buffer.atomicGet()));
        System.out.println();
        for (double i = 0; i < smallSize + 15; i++) {
            System.out.println("Push: " + i);
            smallCircularBuffer.push(i);
            System.out.println(Arrays.toString(smallCircularBuffer.buffer.atomicGet()));
        }
        System.out.println("Is empty: " + smallCircularBuffer.isEmpty());
        System.out.println("Is full: " + smallCircularBuffer.isFull());
        System.out.println();
        for (int i = 0; i < smallSize; i++) {
            System.out.println("Pop: " + smallCircularBuffer.pop());
            System.out.println(Arrays.toString(smallCircularBuffer.buffer.atomicGet()));
        }
        System.out.println("Is empty: " + smallCircularBuffer.isEmpty());
        System.out.println("Is full: " + smallCircularBuffer.isFull());
    }
}
