package com.epam.courses.parallel_computing.blocking_circular_buffer;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/30/2014
 * Time: 7:23 PM
 */
public class BlockingCircularBufferWithoutLocks<T> extends BlockingCircularBuffer<T> {

    public BlockingCircularBufferWithoutLocks(int size) {
        super(size);
    }

    @Override
    public synchronized T take() {
        while (isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        T result = buffer[first];
        buffer[first] = null;
        first = increasedIndex(first);
        notifyAll();
        return result;
    }

    @Override
    public synchronized void put(T element) {
        while (isFull()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        buffer[last] = element;
        last = increasedIndex(last);
        notifyAll();
    }

    public static void main(String[] args) {
        int size = 5;
        BlockingCircularBufferWithoutLocks<Double>
                blockingCircularBufferWithoutLocks = new BlockingCircularBufferWithoutLocks<>(size);
        for (double i = 0; i < size; i++) {
            blockingCircularBufferWithoutLocks.put(i);
            System.out.println("Put: " + i);
        }
        System.out.println(Arrays.toString(blockingCircularBufferWithoutLocks.buffer));
        for (int i = 0; i < size; i++) {
            System.out.println("Taken: " + blockingCircularBufferWithoutLocks.take());
        }
        System.out.println(Arrays.toString(blockingCircularBufferWithoutLocks.buffer));
        for (double i = 0; i < size; i++) {
            System.out.println("Put: " + i);
            blockingCircularBufferWithoutLocks.put(i);
            System.out.println(Arrays.toString(blockingCircularBufferWithoutLocks.buffer));
        }
        for (int i = 0; i < size; i++) {
            System.out.println("Taken: " + blockingCircularBufferWithoutLocks.take());
            System.out.println(Arrays.toString(blockingCircularBufferWithoutLocks.buffer));
        }
    }
}
