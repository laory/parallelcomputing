package com.epam.courses.parallel_computing.blocking_circular_buffer;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/1/2014
 * Time: 11:21 AM
 */
public abstract class BlockingCircularBuffer<T> {

    private final int size;

    protected final T[] buffer;

    protected int first = 0;
    protected int last  = 0;

    @SuppressWarnings("unchecked")
    public BlockingCircularBuffer(int size) {
        buffer = (T[]) (new Object[size + 1]);
        this.size = size;
    }

    public abstract void put(T element);

    public abstract T take();

    public boolean isEmpty() {
        return last == first;
    }

    public boolean isFull() {
        return increasedIndex(last) == first;
    }

    protected int increasedIndex(int index) {
        return (index + 1) % (size + 1);
    }

    @Override
    public String toString() {
        return "BlockingCircularBuffer{" +
               "buffer=" + Arrays.toString(buffer) +
               '}';
    }
}
