package com.epam.courses.parallel_computing.blocking_circular_buffer;

import java.util.Arrays;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/1/2014
 * Time: 11:04 AM
 */

public class BlockingCircularBufferWithLocks<T> extends BlockingCircularBuffer<T> {

    private final Lock      lock     = new ReentrantLock();
    private final Condition notEmpty = lock.newCondition();
    private final Condition notFull  = lock.newCondition();

    public BlockingCircularBufferWithLocks(int size) {
        super(size);
    }

    @Override
    public T take() {
        lock.lock();
        try {
            while (isEmpty()) {
                notEmpty.await();
            }
            T result = buffer[first];
            buffer[first] = null;
            first = increasedIndex(first);
            notFull.signal();
            return result;
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new IllegalStateException("Not empty condition failure", e);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void put(T element) {
        lock.lock();
        try {
            while (isFull()) {
                notFull.await();
            }
            buffer[last] = element;
            last = increasedIndex(last);
            notEmpty.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new IllegalStateException("Not full condition failure", e);
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        int size = 5;
        BlockingCircularBufferWithLocks<Double> blockingCircularBuffer = new BlockingCircularBufferWithLocks<>(size);
        for (double i = 0; i < size; i++) {
            blockingCircularBuffer.put(i);
            System.out.println("Put: " + i);
        }
        System.out.println(Arrays.toString(blockingCircularBuffer.buffer));
        for (int i = 0; i < size; i++) {
            System.out.println("Taken: " + blockingCircularBuffer.take());
        }
        System.out.println(Arrays.toString(blockingCircularBuffer.buffer));
        for (double i = 0; i < size; i++) {
            System.out.println("Put: " + i);
            blockingCircularBuffer.put(i);
            System.out.println(Arrays.toString(blockingCircularBuffer.buffer));
        }
        for (int i = 0; i < size; i++) {
            System.out.println("Taken: " + blockingCircularBuffer.take());
            System.out.println(Arrays.toString(blockingCircularBuffer.buffer));
        }
    }
}
