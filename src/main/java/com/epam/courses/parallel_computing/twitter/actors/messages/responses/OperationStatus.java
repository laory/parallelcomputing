package com.epam.courses.parallel_computing.twitter.actors.messages.responses;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/14/2014
 * Time: 10:50 AM
 */
public enum OperationStatus {

    Ok, Failed;
}