package com.epam.courses.parallel_computing.twitter.models;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/15/2014
 * Time: 10:23 AM
 */
public class Tweet implements Serializable, Comparable {

    private static final long serialVersionUID = 100500L;

    public final String text;
    public final long   initialTimeInMillis;

    public Tweet(String text, long initialTimeInMillis) {
        this.text = text;
        this.initialTimeInMillis = initialTimeInMillis;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", text, timeDifferenceToString());
    }

    @Override
    public int compareTo(Object o) {
        if (o == null || getClass() != o.getClass()) {
            throw new IllegalArgumentException("Incompatible types");
        }
        if (this == o) {
            return 0;
        }
        Tweet that = (Tweet) o;
        // desc order
        if (this.initialTimeInMillis > that.initialTimeInMillis) {
            return -1;
        } else if (this.initialTimeInMillis < that.initialTimeInMillis) {
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Tweet that = (Tweet) o;

        if (this.initialTimeInMillis != that.initialTimeInMillis) {
            return false;
        }
        if (this.text != null ? !this.text.equals(that.text) : that.text != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = text != null ? text.hashCode() : 0;
        result = 31 * result + (int) (initialTimeInMillis ^ (initialTimeInMillis >>> 32));
        return result;
    }

    private String timeDifferenceToString() {
        long currentTimeInMillis = System.currentTimeMillis();
        long timeDifference = currentTimeInMillis - initialTimeInMillis;
        if ((timeDifference /= 1000) < 60) {
            return timeDifference + " seconds ago";
        } else if ((timeDifference /= 60) < 60) {
            return timeDifference + " minutes ago";
        } else {
            return timeDifference / 60 + " hours ago";
        }
    }
}
