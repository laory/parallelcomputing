package com.epam.courses.parallel_computing.twitter.actors.messages.operations;

import com.epam.courses.parallel_computing.twitter.models.User;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/14/2014
 * Time: 10:52 AM
 */
public abstract class Operation implements Serializable {

    private static final long serialVersionUID = 100500L;

    public final User user;

    protected Operation(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "user=" + user;
    }
}