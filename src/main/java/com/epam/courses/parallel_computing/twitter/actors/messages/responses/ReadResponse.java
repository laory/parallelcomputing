package com.epam.courses.parallel_computing.twitter.actors.messages.responses;

import com.epam.courses.parallel_computing.twitter.models.Tweet;
import com.epam.courses.parallel_computing.twitter.models.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/14/2014
 * Time: 11:05 AM
 */
public class ReadResponse extends OperationResponse {

    public final User        user;
    public final List<Tweet> userTweets;

    public ReadResponse(OperationStatus status, User user, List<Tweet> userTweets) {
        this(status, status.toString(), user, userTweets);
    }

    public ReadResponse(OperationStatus status, String message, User user) {
        this(status, message, user, null);
    }

    public ReadResponse(OperationStatus status, String message, User user, List<Tweet> userTweets) {
        super(status, message);
        this.userTweets = userTweets;
        this.user = user;
    }

    @Override
    public String toString() {
        return "ReadResponse{" +
               "user=" + user +
               ", userTweets=" + userTweets +
               "} " + super.toString();
    }
}
