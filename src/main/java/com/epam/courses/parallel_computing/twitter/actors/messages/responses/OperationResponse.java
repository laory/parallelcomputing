package com.epam.courses.parallel_computing.twitter.actors.messages.responses;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/14/2014
 * Time: 11:18 AM
 */
public class OperationResponse implements Serializable {

    private static final long serialVersionUID = 100500L;

    public final OperationStatus operationStatus;
    public final String          message;

    public OperationResponse(OperationStatus operationStatus, String message) {
        this.operationStatus = operationStatus;
        this.message = message;
    }

    @Override
    public String toString() {
        return "OperationResponse{" +
               "operationStatus=" + operationStatus +
               ", message='" + message + '\'' +
               '}';
    }
}
