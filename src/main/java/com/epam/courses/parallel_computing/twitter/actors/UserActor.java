package com.epam.courses.parallel_computing.twitter.actors;

import akka.actor.UntypedActor;
import com.epam.courses.parallel_computing.twitter.actors.messages.operations.*;
import com.epam.courses.parallel_computing.twitter.actors.messages.responses.OperationResponse;
import com.epam.courses.parallel_computing.twitter.actors.messages.responses.OperationStatus;
import com.epam.courses.parallel_computing.twitter.actors.messages.responses.ReadResponse;
import com.epam.courses.parallel_computing.twitter.actors.messages.responses.WallResponse;
import com.epam.courses.parallel_computing.twitter.models.Tweet;
import com.epam.courses.parallel_computing.twitter.models.User;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/14/2014
 * Time: 1:56 PM
 */
public class UserActor extends UntypedActor {

    private static final String POST_COMMAND    = " -> ";
    private static final String FOLLOWS_COMMAND = " follows ";
    private static final String WALL_COMMAND    = " wall";

    @Override
    public void preStart() throws Exception {
        System.out.println("User actor initialized...");
    }

    @Override
    public void onReceive(Object receivedMsg) throws Exception {
//        System.out.println("User actor received message " + receivedMsg);
        if (receivedMsg instanceof String) {
            handleInput((String) receivedMsg);
        } else if (receivedMsg instanceof OperationResponse) {
            handleOperationResponse((OperationResponse) receivedMsg);
        } else {
            unhandled(receivedMsg);
        }
    }

    private void handleInput(String receivedMsg) {
        Operation operation;
        if (receivedMsg.contains(POST_COMMAND)) {
            String[] postProperties = receivedMsg.split(POST_COMMAND);
            User user = new User(postProperties[0]);
            String text = postProperties[1];
            long initialTimeInMillis = System.currentTimeMillis();
            Tweet tweet = new Tweet(text, initialTimeInMillis);
            operation = new Post(user, tweet);
        } else if (receivedMsg.contains(FOLLOWS_COMMAND)) {
            String[] postProperties = receivedMsg.split(FOLLOWS_COMMAND);
            User followingUser = new User(postProperties[0]);
            User followedUser = new User(postProperties[1]);
            operation = new Follow(followingUser, followedUser);
        } else if (receivedMsg.contains(WALL_COMMAND)) {
            String[] postProperties = receivedMsg.split(WALL_COMMAND);
            User user = new User(postProperties[0]);
            operation = new Wall(user);
        } else {
            operation = new Read(new User(receivedMsg));
        }
        getSender().tell(operation, getSelf());
    }

    private void handleOperationResponse(OperationResponse receivedMsg) {
        if (receivedMsg.operationStatus == OperationStatus.Ok) {
            if (receivedMsg instanceof ReadResponse) {
                ReadResponse readResponse = (ReadResponse) receivedMsg;
                System.out.println();
                for (Tweet userTweet : readResponse.userTweets) {
                    System.out.println(userTweet);
                }
                System.out.println();
            } else if (receivedMsg instanceof WallResponse) {
                WallResponse wallResponse = (WallResponse) receivedMsg;
                System.out.println();
                for (Map.Entry<Tweet, User> tweet2User : wallResponse.wallPosts.entrySet()) {
                    System.out.println(tweet2User.getValue() + " - " + tweet2User.getKey());
                }
                System.out.println();
            }
        } else {
            System.err.println(receivedMsg.operationStatus + ": " + receivedMsg.message);
        }
    }
}