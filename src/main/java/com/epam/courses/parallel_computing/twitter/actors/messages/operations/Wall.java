package com.epam.courses.parallel_computing.twitter.actors.messages.operations;

import com.epam.courses.parallel_computing.twitter.models.User;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/13/2014
 * Time: 12:53 PM
 */
public class Wall extends Operation {

    public Wall(User user) {
        super(user);
    }

    @Override
    public String toString() {
        return "Wall{" +
               super.toString() +
               '}';
    }
}
