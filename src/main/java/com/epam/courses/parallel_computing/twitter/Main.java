package com.epam.courses.parallel_computing.twitter;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.epam.courses.parallel_computing.twitter.actors.GlobalActor;
import scala.concurrent.duration.Duration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/13/2014
 * Time: 11:54 AM
 */
public class Main {

    private static final ActorSystem twitterActorSystem = ActorSystem.create("TwitterActorSystem");
    private static final String      EXIT_COMMAND       = "q";

    public static void main(String[] args) {

        ActorRef globalActorSupervisor = twitterActorSystem.actorOf(Props.create(GlobalActor.class));

        System.out.println("Twitter initialization... (Quit by typing q command)");

        try (BufferedReader bufferedConsoleReader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String readLine = bufferedConsoleReader.readLine();
                if (readLine.equals(EXIT_COMMAND)) {
                    stop();
                    break;
                } else {
                    globalActorSupervisor.tell(readLine, ActorRef.noSender());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            stop();
            System.exit(1);
        }
    }

    private static void stop() {
        twitterActorSystem.shutdown();
        twitterActorSystem.awaitTermination(Duration.create(10, TimeUnit.SECONDS));
    }
}
