package com.epam.courses.parallel_computing.twitter.actors.messages.responses;

import com.epam.courses.parallel_computing.twitter.models.Tweet;
import com.epam.courses.parallel_computing.twitter.models.User;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/14/2014
 * Time: 11:58 AM
 */
public class WallResponse extends OperationResponse {

    public final User             user;
    public final Map<Tweet, User> wallPosts;

    public WallResponse(OperationStatus operationStatus, String message, User user) {
        this(operationStatus, message, user, null);
    }

    public WallResponse(OperationStatus operationStatus, User user, Map<Tweet, User> wallPosts) {
        this(operationStatus, operationStatus.toString(), user, wallPosts);
    }

    public WallResponse(OperationStatus operationStatus, String message, User user, Map<Tweet, User> wallPosts) {
        super(operationStatus, message);
        this.user = user;
        this.wallPosts = wallPosts;
    }

    @Override
    public String toString() {
        return "WallResponse{" +
               "user=" + user +
               ", wallPosts=" + wallPosts +
               "} " + super.toString();
    }
}
