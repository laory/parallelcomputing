package com.epam.courses.parallel_computing.twitter.actors.messages.operations;

import com.epam.courses.parallel_computing.twitter.models.Tweet;
import com.epam.courses.parallel_computing.twitter.models.User;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/13/2014
 * Time: 12:53 PM
 */
public class Post extends Operation {

    public final Tweet tweet;

    public Post(User user, Tweet tweet) {
        super(user);
        this.tweet = tweet;
    }

    @Override
    public String toString() {
        return "Post{" +
               "tweet=" + tweet + ", " +
               super.toString() +
               '}';
    }
}
