package com.epam.courses.parallel_computing.twitter.actors;

import akka.actor.UntypedActor;
import com.epam.courses.parallel_computing.twitter.actors.messages.operations.Follow;
import com.epam.courses.parallel_computing.twitter.actors.messages.operations.Post;
import com.epam.courses.parallel_computing.twitter.actors.messages.operations.Read;
import com.epam.courses.parallel_computing.twitter.actors.messages.operations.Wall;
import com.epam.courses.parallel_computing.twitter.actors.messages.responses.OperationResponse;
import com.epam.courses.parallel_computing.twitter.actors.messages.responses.ReadResponse;
import com.epam.courses.parallel_computing.twitter.actors.messages.responses.WallResponse;
import com.epam.courses.parallel_computing.twitter.models.Tweet;
import com.epam.courses.parallel_computing.twitter.models.User;

import java.util.*;

import static com.epam.courses.parallel_computing.twitter.actors.messages.responses.OperationStatus.Failed;
import static com.epam.courses.parallel_computing.twitter.actors.messages.responses.OperationStatus.Ok;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/13/2014
 * Time: 12:03 PM
 */
public class TwitterActor extends UntypedActor {

    private final Map<User, List<Tweet>> tweets  = new HashMap<>();
    private final Map<User, Set<User>>   follows = new HashMap<>();

    @Override
    public void preStart() throws Exception {
        System.out.println("Twitter actor initialized...");
    }

    @Override
    public void onReceive(Object receivedMsg) throws Exception {
//        System.out.println("Twitter actor received message " + receivedMsg);
        if (receivedMsg instanceof Post) {
            handlePostMessage((Post) receivedMsg);
        } else if (receivedMsg instanceof Read) {
            handleReadMessage((Read) receivedMsg);
        } else if (receivedMsg instanceof Follow) {
            handleFollowMessage((Follow) receivedMsg);
        } else if (receivedMsg instanceof Wall) {
            handleWallMessage((Wall) receivedMsg);
        } else {
            unhandled(receivedMsg);
        }
    }

    private void handlePostMessage(Post receivedMsg) {
        List<Tweet> userTweets = tweets.get(receivedMsg.user);
        if (userTweets != null) {
            userTweets.add(receivedMsg.tweet);
        } else {
            userTweets = new ArrayList<>();
            userTweets.add(receivedMsg.tweet);
            tweets.put(receivedMsg.user, userTweets);
        }
        getSender().tell(new OperationResponse(Ok, String.format("User {%s} posted tweet {%s} successfully",
                                                                 receivedMsg.user, receivedMsg.tweet)), getSelf());
    }

    private void handleReadMessage(Read receivedMsg) {
        ReadResponse readResponse;
        if (tweets.containsKey(receivedMsg.user)) {
            List<Tweet> userTweets = tweets.get(receivedMsg.user);
            Collections.sort(userTweets);
            readResponse = new ReadResponse(Ok, receivedMsg.user, new ArrayList<>(userTweets));
        } else {
            readResponse = new ReadResponse(Failed, String.format("Specified user {%s} is not registered",
                                                                        receivedMsg.user), receivedMsg.user);
        }
        getSender().tell(readResponse, getSelf());
    }

    private void handleFollowMessage(Follow receivedMsg) {
        OperationResponse response;
        if (tweets.containsKey(receivedMsg.user) && tweets.containsKey(receivedMsg.followedUser)) {
            Set<User> userFollows = follows.get(receivedMsg.user);
            if (userFollows == null) {
                userFollows = new HashSet<>();
            }
            userFollows.add(receivedMsg.followedUser);
            follows.put(receivedMsg.user, userFollows);
            response = new OperationResponse(Ok, String.format("User {%s} follows user {%s}", receivedMsg.user,
                                                               receivedMsg.followedUser));
        } else {
            response = new OperationResponse(Failed, String.format("Some of specified users {%s, %s} is not registered",
                                                                   receivedMsg.user, receivedMsg.followedUser));
        }
        getSender().tell(response, getSelf());
    }

    private void handleWallMessage(Wall receivedMsg) {
        WallResponse response;
        Set<User> userFollows = follows.get(receivedMsg.user);
        List<Tweet> userTweets = tweets.get(receivedMsg.user);
        if (userTweets == null) {
            response = new WallResponse(Failed, String.format("User {%s} is not registered", receivedMsg.user),
                                        receivedMsg.user);
        } else {
            Map<Tweet, User> sortedTweets = new TreeMap<>();
            for (Tweet userTweet : userTweets) {
                sortedTweets.put(userTweet, receivedMsg.user);
            }
            if (userFollows != null) {
                for (User followedUser : userFollows) {
                    List<Tweet> followedUserTweets = tweets.get(followedUser);
                    if (followedUserTweets != null) {
                        for (Tweet followedUserTweet : followedUserTweets) {
                            sortedTweets.put(followedUserTweet, followedUser);
                        }
                    }
                }
            }
            response = new WallResponse(Ok, receivedMsg.user, sortedTweets);
        }
        getSender().tell(response, getSelf());
    }
}
