package com.epam.courses.parallel_computing.twitter.actors;

import com.epam.courses.parallel_computing.twitter.actors.messages.operations.Operation;
import akka.actor.*;
import akka.japi.Function;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

import static akka.actor.SupervisorStrategy.restart;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/15/2014
 * Time: 12:06 PM
 */
public class GlobalActor extends UntypedActor {

    private static final SupervisorStrategy strategy = new OneForOneStrategy(10, Duration.create(1, TimeUnit.MINUTES),
                                                                             new Function<Throwable, SupervisorStrategy.Directive>() {
                                                                                 @Override
                                                                                 public SupervisorStrategy.Directive apply(
                                                                                         Throwable throwable) throws
                                                                                                              Exception {
                                                                                     return restart();
                                                                                 }
                                                                             });

    private ActorRef twitterActor;
    private ActorRef userActor;

    @Override
    public void preStart() throws Exception {
        System.out.println("Global actor initialized...");

        twitterActor = getContext().actorOf(Props.create(TwitterActor.class));
        userActor = getContext().actorOf(Props.create(UserActor.class));

        getContext().watch(twitterActor);
        getContext().watch(userActor);
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

    @Override
    public void onReceive(Object receivedMsg) throws Exception {
        //        System.out.println("Global actor received message " + receivedMsg);
        if (receivedMsg instanceof String) {
            userActor.tell(receivedMsg, getSelf());
        } else if (receivedMsg instanceof Operation) {
            twitterActor.forward(receivedMsg, getContext());
        } else {
            unhandled(receivedMsg);
        }
    }
}
