package com.epam.courses.parallel_computing.sin_convergent_series.executors;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/25/2014
 * Time: 11:21 AM
 */
public interface ConvergentSeriesExecutor<T> {

    public T calculateConvergentSeries();
}
