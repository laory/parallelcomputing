package com.epam.courses.parallel_computing.sin_convergent_series.executors;

import com.epam.courses.parallel_computing.sin_convergent_series.calculators.SinSumCalculatorThread;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/24/2014
 * Time: 10:32 AM
 */
public class PlainThreadArray implements ConvergentSeriesExecutor<Double> {

    private final SinSumCalculatorThread[] sinCalculators;

    public PlainThreadArray(int n, int threads) {
        sinCalculators = new SinSumCalculatorThread[threads];

        int iterations = 2 * n + 1;
        int threadIterations = iterations / threads;
        int argument = -n;

        for (int i = 0; i < threads - 1; i++) {
            sinCalculators[i] = new SinSumCalculatorThread(argument, (argument += threadIterations) - 1);
        }

        sinCalculators[threads - 1] = new SinSumCalculatorThread(argument, n);
    }

    @Override
    public Double calculateConvergentSeries() {
        double sum = 0;

        for (SinSumCalculatorThread calculationThread : sinCalculators) {
            calculationThread.start();
        }

        for (SinSumCalculatorThread calculationThread : sinCalculators) {
            try {
                calculationThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (SinSumCalculatorThread calculationThread : sinCalculators) {
            sum += calculationThread.getSum();
        }

        return sum;
    }

    @Override
    public String toString() {
        return "PlainThreadArray{" +
               "sinCalculators=" + Arrays.toString(sinCalculators) +
               '}';
    }
}
