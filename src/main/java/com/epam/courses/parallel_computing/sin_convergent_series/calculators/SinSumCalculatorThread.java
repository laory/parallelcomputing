package com.epam.courses.parallel_computing.sin_convergent_series.calculators;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/25/2014
 * Time: 10:49 AM
 */
public class SinSumCalculatorThread extends Thread {

    private final int lowerArgument;
    private final int higherArgument;

    private double sum;

    public SinSumCalculatorThread(int lowerArgument, int higherArgument) {
        this.sum = 0;
        this.lowerArgument = lowerArgument;
        this.higherArgument = higherArgument;
    }

    public double getSum() {
        return sum;
    }

    @Override
    public void run() {
        for (int i = lowerArgument; i <= higherArgument; i++) {
            sum += Math.sin(i);
        }
//        System.out.println(lowerArgument + " " + higherArgument + " " + sum);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        String delimiter = "";
        for (int i = lowerArgument; i <= higherArgument; i++) {
            stringBuilder.append(delimiter);
            stringBuilder.append(i);
            delimiter = ", ";
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}