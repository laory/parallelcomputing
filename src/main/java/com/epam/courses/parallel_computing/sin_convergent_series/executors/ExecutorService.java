package com.epam.courses.parallel_computing.sin_convergent_series.executors;

import com.epam.courses.parallel_computing.sin_convergent_series.calculators.SinSumCalculatorCallable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/24/2014
 * Time: 10:32 AM
 */
public class ExecutorService implements ConvergentSeriesExecutor<Double> {

    private final java.util.concurrent.ExecutorService sinSumCalculatorService;
    private final List<SinSumCalculatorCallable>       calculatorsList;

    public ExecutorService(int n, int threads, int threadPoolSize) {

        int iterations = 2 * n + 1;
        int calculatorIterations = iterations / threads;
        int argument = -n;

        calculatorsList = new ArrayList<>();
        for (int i = 0; i < threads - 1; i++) {
            calculatorsList.add(new SinSumCalculatorCallable(argument, (argument += calculatorIterations) - 1));
        }
        calculatorsList.add(new SinSumCalculatorCallable(argument, n));

        sinSumCalculatorService = Executors.newFixedThreadPool(threadPoolSize);
    }

    @Override
    public Double calculateConvergentSeries() {
        double sum = 0;
        try {
            List<Future<Double>> calculationFutures = sinSumCalculatorService.invokeAll(calculatorsList);
            for (Future<Double> calculationFuture : calculationFutures) {
                sum += calculationFuture.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            sinSumCalculatorService.shutdown(); // Disable new tasks from being submitted
            try {
                // Wait a while for existing tasks to terminate
                if (!sinSumCalculatorService.awaitTermination(60, TimeUnit.SECONDS)) {
                    sinSumCalculatorService.shutdownNow(); // Cancel currently executing tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!sinSumCalculatorService.awaitTermination(60, TimeUnit.SECONDS)) {
                        System.err.println("Pool did not terminate");
                    }
                }
            } catch (InterruptedException ie) {
                // (Re-)Cancel if current thread also interrupted
                sinSumCalculatorService.shutdownNow();
                // Preserve interrupt status
                Thread.currentThread().interrupt();
            }
        }
        return sum;
    }

    @Override
    public String toString() {
        return "ExecutorService{" +
               "sinSumCalculatorService=" + sinSumCalculatorService +
               ", calculatorsList=" + calculatorsList +
               '}';
    }
}
