package com.epam.courses.parallel_computing.sin_convergent_series;

import com.epam.courses.parallel_computing.sin_convergent_series.executors.ConvergentSeriesExecutor;
import com.epam.courses.parallel_computing.sin_convergent_series.executors.ExecutorService;
import com.epam.courses.parallel_computing.sin_convergent_series.executors.PlainThreadArray;

import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/25/2014
 * Time: 12:00 PM
 */
public class SinConvergentSeries {

    ConvergentSeriesExecutor<Double> convergentSeriesExecutor;

    public SinConvergentSeries(ConvergentSeriesExecutor convergentSeriesExecutor) {
        this.convergentSeriesExecutor = convergentSeriesExecutor;
    }

    public double calculate() {
        return convergentSeriesExecutor.calculateConvergentSeries();
    }

    @Override
    public String toString() {
        return "SinConvergentSeries{" +
               "convergentSeriesExecutor=" + convergentSeriesExecutor +
               '}';
    }

    public static void main(String[] args) {
        int threads = 1000;
        int n = 1_000_000;
        int threadPoolSize = 0;

//        ConvergentSeriesExecutor plainThreadArray = new PlainThreadArray(n, threads);
//        SinConvergentSeries sinConvergentSeries = new SinConvergentSeries(plainThreadArray);

        threadPoolSize = 100;
        ConvergentSeriesExecutor executorService = new ExecutorService(n, threads, threadPoolSize);
        SinConvergentSeries sinConvergentSeries = new SinConvergentSeries(executorService);

//        System.out.println(sinConvergentSeries);

        long start = System.currentTimeMillis();
        double sum = sinConvergentSeries.calculate();
        long executionTime = System.currentTimeMillis() - start;

        System.out.println("SIN CONVERGENT SERIES is " + sum);
        System.out.println("N " + n);
        System.out.println("THREADS " + threads);
        System.out.println("THREAD POOL SIZE " + threadPoolSize);
        System.out.println("TIME " + new DecimalFormat("#.00").format(executionTime / 1000.0) + "s");

    }
}
