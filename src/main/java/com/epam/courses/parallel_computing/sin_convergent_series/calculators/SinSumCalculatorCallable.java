package com.epam.courses.parallel_computing.sin_convergent_series.calculators;

import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 9/25/2014
 * Time: 10:49 AM
 */
public class SinSumCalculatorCallable implements Callable<Double> {

    private final int lowerArgument;
    private final int higherArgument;

    public SinSumCalculatorCallable(int lowerArgument, int higherArgument) {
        this.lowerArgument = lowerArgument;
        this.higherArgument = higherArgument;
    }

    @Override
    public Double call() {
        double sum = 0.0;
        for (int i = lowerArgument; i <= higherArgument; i++) {
            sum += Math.sin(i);
        }
//        System.out.println(lowerArgument + " " + higherArgument + " " + sum);
        return sum;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        String delimiter = "";
        for (int i = lowerArgument; i <= higherArgument; i++) {
            stringBuilder.append(delimiter);
            stringBuilder.append(i);
            delimiter = ", ";
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}