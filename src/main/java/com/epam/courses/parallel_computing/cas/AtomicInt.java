package com.epam.courses.parallel_computing.cas;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/28/2016
 * Time: 12:07 PM
 */
public class AtomicInt {

    private CasIntPrimitive primitive = new CasIntPrimitive(0);

    public int getValue() {
        return primitive.getValue();
    }

    public int increment() {
        int currentValue;
        int newValue;
        do {
            currentValue = primitive.getValue();
            newValue = currentValue + 1;
        } while (!primitive.compareAndSwap(currentValue, newValue));
        return newValue;
    }
}

class CasIntPrimitive {

    private volatile int value;

    public CasIntPrimitive(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public synchronized boolean compareAndSwap(int expectedValue, int newValue) {
        if (value == expectedValue) {
            value = newValue;
            return true;
        }
        return false;
    }
}
