package com.epam.courses.parallel_computing.bank;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/1/2014
 * Time: 11:37 AM
 */
public class Account {

    public final Lock lock = new ReentrantLock();
    public final int sn;

    private volatile int money;

    public Account(int sn, int money) {
        this.sn = sn;
        this.money = money;
    }

    public synchronized void withdraw(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Wrong number to withdraw. Amount: " + amount);
        }
//        if (money < amount) {
//            throw new IllegalArgumentException("Not enough money to perform withdraw. Amount: " + amount);
//        }
        money -= amount;
    }

    public synchronized void deposit(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Wrong number to deposit. Amount: " + amount);
        }
        money += amount;
    }

    @Override
    public String toString() {
        return "Account{" +
               "sn=" + sn +
               ", money=" + money +
               '}';
    }
}
