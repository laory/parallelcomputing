package com.epam.courses.parallel_computing.bank;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/1/2014
 * Time: 11:34 AM
 */
public class Bank {

    private final Account[] accounts;

    public Bank(Account[] accounts) {
        this.accounts = accounts;
    }

    public Account getAccount(int accountSn) {
        for (Account account : accounts) {
            if (account.sn == accountSn) {
                return account;
            }
        }
        throw new IllegalArgumentException("Not existed account. S/n: " + accountSn);
    }

    public void transfer(Account from, Account to, int amount) throws InterruptedException {
        boolean finished = false;
        int tries = 5;
//        System.out.println("Thread: " + Thread.currentThread().getId() + " started. " +
//                           from.sn + " -> " + to.sn + " (" + amount + ")");
        while (!finished && tries-- > 0) {
            if (from.lock.tryLock()) {
                try {
                    if (to.lock.tryLock()) {
                        try {
                            from.withdraw(amount);
                            to.deposit(amount);
                            finished = true;
                            Thread.sleep(5);
                        } finally {
                            to.lock.unlock();
                        }
                    }
                } finally {
                    from.lock.unlock();
                }
            }
            Thread.sleep(5);
        }
        if (!finished && tries == 0) {
            throw new IllegalMonitorStateException("Wasn't able to lock on accounts: " + from.sn + ", " + to.sn);
        }
//        System.out.println("Thread: " + Thread.currentThread().getId() + " finished. " +
//                           from + " " + to);
    }

    public static void main(String[] args) throws InterruptedException {
        for (int k = 0; k < 100; k++) {
            final Account accountA = new Account(0, 100050);
            final Account accountB = new Account(1, 522236356);
            final Account accountC = new Account(2, 23450);
            final Account[] accounts = new Account[] {accountA, accountB};
            final Bank bank = new Bank(accounts);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    accountA.withdraw(1); // 100049
                    accountB.deposit(1); // 101
                }
            }).start();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    accountA.withdraw(1); // 100048
                    accountB.deposit(1); // 102
                }
            }).start();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    accountA.withdraw(1); // 100047
                    accountA.deposit(4); // 100051
                    accountB.deposit(1); // 103
                }
            }).start();
            accountB.deposit(1); // 104
            List<Thread> threads = new ArrayList<>();
            for (int i = 0; i < 1000; i++) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            bank.transfer(accountA, accountB, 5); // 95051 5104
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                threads.add(thread);
            }
            List<Thread> threads2 = new ArrayList<>();
            for (int i = 0; i < 1000; i++) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            bank.transfer(accountC, accountB, 1); // 95051 5104
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                threads2.add(thread);
            }
            List<Thread> threads3 = new ArrayList<>();
            for (int i = 0; i < 1000; i++) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            bank.transfer(accountB, accountA, 12); // 95051 5104
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                threads3.add(thread);
            }
            for (Thread thread : threads) {
                thread.start();
            }
            for (Thread thread : threads2) {
                thread.start();
            }
            for (Thread thread : threads3) {
                thread.start();
            }
            for (Thread thread : threads) {
                thread.join();
            }
            for (Thread thread : threads2) {
                thread.join();
            }
            for (Thread thread : threads3) {
                thread.join();
            }
            accountA.withdraw(1); // 95050
            accountA.deposit(4); // 95054
            System.out.println(accountA);
            System.out.println(accountB);
            System.out.println(accountC);
        }
    }
}
