package com.epam.courses.parallel_computing.cas;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 10/28/2016
 * Time: 12:39 PM
 */
public class AtomicIntTest {

    public static final int THREAD_COUNT = 100;
    public static final int OPERATIONS_PER_THREAD = 1_000_000;
    private AtomicInt atomicInt;
    private AtomicInteger javaAtomicInt;

    @Before
    public void setUp() throws Exception {
        atomicInt = new AtomicInt();
        javaAtomicInt = new AtomicInteger();
    }

    @Test
    public void increment() throws Exception {
        List<Thread> threads = IntStream.range(0, THREAD_COUNT).mapToObj(i -> new Thread(() -> {
            IntStream.range(0, OPERATIONS_PER_THREAD).forEach(j -> atomicInt.increment());
            IntStream.range(0, OPERATIONS_PER_THREAD).forEach(j -> javaAtomicInt.incrementAndGet());
        })).collect(Collectors.toList());

        threads.forEach(Thread:: start);
        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                e.printStackTrace();
            }
        });

        assertEquals(THREAD_COUNT * OPERATIONS_PER_THREAD, atomicInt.getValue());
        assertEquals(javaAtomicInt.get(), atomicInt.getValue());
    }
}